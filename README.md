# Affari-Microsite with SASS-GULP
Microsite for \client/

go to your new project folder: open terminal and type:

$ git clone https://bitbucket.org/affarimedia/affari-boilerplate-html-sass.git

- delete folder .git,
- npm install


---------
commands:
---------

start developer
$ npm run watch

production mode
$ npm run build

clean dist folder
$ npm run clean
-----
