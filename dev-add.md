==============================
==============================
// Optimizing Images
gulp.task('images2', function() {
  return gulp.src('app/media/**/*.+(png|jpg|jpeg|gif|svg)')
    // Caching images that ran through imagemin
    .pipe(cache(imagemin({
      interlaced: true,
    })))
    .pipe(gulp.dest('dist/media'))
});
